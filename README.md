# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Front End development of a website. I made it sole for the purpose of my portfolio.
Feel free to use the code and change it anyway you want, 100% free, no contribution required.

Fully responsive layout; Based on Flexbox layout mode; HTML5; SCSS / Sass; FontAwesome icons; Owl Carousel; Masonry; Filtering items; Google Map integrated; Google Web Fonts (Montserrat)

The design was made by Illia Nesterov. For the terms of using the design, visit his portfolio here: https://www.behance.net/gallery/35699887/Clemo-Free-PSD-Template.

### How do I get set up? ###

I reccomend using Brackets + Prepros compiler, but it's on you :-)

### Contribution guidelines ###

You can contribute if you think it would be useful and/or fun.

The only thing I ask is: please do not edit CSS file directly. Compile the SCSS files. Thank you!

### Who do I talk to? ###

You can contact me on contact@tamarajug.com