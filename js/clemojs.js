$(document).ready(function () {
    // NAVIGATION
    $('#nav-icon').click(function () {
        $(this).toggleClass('open');
    });
    $("#nav-icon").click(function () {
        $("nav").slideToggle("slow");
    });
    // HEADER SCROLL
    var header = $('header');
    header.data('size', 'big');
    $(window).scroll(function () {
        if ($(document).scrollTop() > 0) {
            if (header.data('size') == 'big') {
                header.data('size', 'small').addClass('header-small');
                header.stop().animate({
                    height: '4em'
                }, 300);
            }
        } else if (header.data('size') == 'small') {
            header.data('size', 'big').removeClass('header-small');
            header.stop().animate({
                height: '8em'
            }, 300);
        }
    });
    // CONTENT MARGIN AFTER HEADER
    var headerHeight = header.height();
    // OWL COVER
    if ($(window).width() < 559) {
        header.next().css('margin-top', '100px');
    } else {
        header.next().css('margin-top', headerHeight);
    }
    $("#owl-cover").owlCarousel({
        navigation: true,
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true
    });
    // OWL WORKS
    $("#owl-works").owlCarousel({
        navigation: true,
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true
    });
    // OWL TEAM
    $("#owl-team").owlCarousel({
        items: 3, // >1000px
        itemsDesktop: [1000, 2], // 1000px - 901px
        itemsDesktopSmall: [900, 2], // 900px - 601px
        itemsTablet: [600, 1], // 600 - 0
        itemsMobile: false // itemsMobile disabled - inherit from itemsTablet option
    });
    $(".next").click(function () {
        $("#owl-team").trigger('owl.next');
    });
    $(".prev").click(function () {
        $("#owl-team").trigger('owl.prev');
    });
    // OWL LOGOS
    $("#owl-logo").owlCarousel({
        items: 4, // >1000px
        itemsDesktop: [1000, 3], // 1000px - 901px
        itemsDesktopSmall: [900, 3], // 900px - 601px
        itemsTablet: [600, 2], // 600 - 0
        itemsMobile: false // itemsMobile disabled - inherit from itemsTablet option
    });
    // OWL INSTAGRAM
    $("#owl-instagram").owlCarousel({
        items: 5, // >1000px
        itemsDesktop: [1300, 4], // 1300px - 1101px
        itemsDesktopSmall: [1100, 3], // 1100px - 831px
        itemsTablet: [830, 1], // 830 - 0
        itemsMobile: false // itemsMobile disabled - inherit from itemsTablet option
    });
    // OWL POSTS
    $("#owl-posts").owlCarousel({
        items: 3, // >1000px
        itemsDesktop: [1000, 2], // 1000px - 901px
        itemsDesktopSmall: [900, 2], // 900px - 601px
        itemsTablet: [600, 1], // 600 - 0
        itemsMobile: false // itemsMobile disabled - inherit from itemsTablet option
    });

    // PORTFOLIO ISOTOPE
    if ($('.isotope-portfolio').length) {

        // init Isotope
        var $grid = $('.isotope-portfolio').isotope({
            itemSelector: '.isotope-item',
            layoutMode: 'masonry'
        });
        // filter functions
        var filterFns = {
            // show if number is greater than 50
            numberGreaterThan50: function () {
                var number = $(this).find('.number').text();
                return parseInt(number, 10) > 50;
            },
        };
        // bind filter button click
        $('.filters-button-group').on('click', 'button', function () {
            var filterValue = $(this).attr('data-filter');
            // use filterFn if matches value
            filterValue = filterFns[filterValue] || filterValue;
            $grid.isotope({
                filter: filterValue
            });
        });
        // change is-checked class on buttons
        $('.button-group').each(function (i, buttonGroup) {
            var $buttonGroup = $(buttonGroup);
            $buttonGroup.on('click', 'button', function () {
                $buttonGroup.find('.is-checked').removeClass('is-checked');
                $(this).addClass('is-checked');
            });
        });

        // PORTFOLIO ITEM HOVER
        var portfolio = $('.portfolio');
        portfolio.on('mouseenter', '.isotope-item', function () {
            $(this).find(".portfolio-hover").fadeIn(400);
        });
        portfolio.on('mouseleave', '.isotope-item', function () {
            $(this).find(".portfolio-hover").fadeOut(400);
        });
    }
});
